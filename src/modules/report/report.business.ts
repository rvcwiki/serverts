// import { Injectable } from '@nestjs/common';
// import { DatabaseService } from 'services/database.service';

// @Injectable()
// export class ReportBusiness {
//     constructor(private readonly databaseServeice: DatabaseService) { }
//     report(id: string): Promise<any> {
//         let reportSQL: string = '';
//         reportSQL = "SELECT e1.PROJECT AS ID, e1.ID AS SSS, CFV.STRINGVALUE AS Epic, e1.SUMMARY AS Epic_Des, SUBSTRING_INDEX(t1.SUMMARY, ':', 1) AS 'Task' ";
//         reportSQL += ", SUBSTRING_INDEX(SUBSTRING_INDEX(t1.SUMMARY, ':', 2), ':', -1) AS Task_des, s1.ID AS Subtask, s1.SUMMARY AS Subtask_des, s1.REPORTER, s1.ASSIGNEE	";
//         reportSQL += ",(SELECT pname FROM issuestatus JS WHERE JS.ID=s1.ISSUESTATUS) STATUS ";
//         reportSQL += "FROM (SELECT JIE.*, ILE.SOURCE, ILE.DESTINATION FROM jiraissue JIE JOIN issuelink ILE ON (ILE.SOURCE = JIE.ID) WHERE JIE.issuetype=10000) e1 LEFT JOIN ";
//         reportSQL += "(SELECT JIT.*, ILT.SOURCE, ILT.DESTINATION FROM jiraissue JIT JOIN issuelink ILT ON (ILT.DESTINATION = JIT.ID)  WHERE JIT.issuetype=10002) t1 ON (t1.ID = e1.DESTINATION) LEFT JOIN ";
//         reportSQL += "(SELECT JIS.*, ILS.SOURCE, ILS.DESTINATION FROM jiraissue JIS JOIN issuelink ILS ON (ILS.DESTINATION = JIS.ID) WHERE JIS.issuetype=10003) s1 ON (s1.SOURCE = t1.DESTINATION) LEFT JOIN customfieldvalue CFV ON (CFV.ISSUE = e1.id) ";
//         reportSQL += "WHERE  e1.PROJECT = " + id + " AND e1.ISSUETYPE = 10000 and CFV.STRINGVALUE not in('sw_team','ghx-label-1','ghx-label-3','10000','User Stories','10400')";
//         return new Promise((resolve, reject) => {
//             this.databaseServeice.getConnection().query(reportSQL, (error, results, fields) => {
//                 if (error) {
//                     reject(error);
//                 }
//                 resolve(results[0]);
//             });
//         })
//     }

// }