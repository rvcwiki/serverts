import { Module } from '@nestjs/common';
import { SearchController } from './search.controller';
import { SearchBusiness } from './search.business';
import { DatabaseService } from '../../services/database.service'
import { ServiceModule } from 'services/service.module';

@Module({
  imports: [ServiceModule],
  controllers: [SearchController],
  providers: [SearchBusiness]
})
export class SearchModule {}
