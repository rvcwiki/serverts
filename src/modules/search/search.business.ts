import { Injectable } from '@nestjs/common';
import { DatabaseService } from '../../services/database.service';

@Injectable()
export class SearchBusiness {
    constructor(private readonly databaseServeice: DatabaseService) { }
    search(dataRequest: any): Promise<any> {
        let sql: any = '';

        if (dataRequest == "Name") {
            sql = "WHERE lower(project.pname) LIKE '%" + dataRequest.toLowerCase().trim() + "%' ";
        } else if (dataRequest == "Type") {
            sql = "WHERE lower(issuetype.pname) LIKE '%" + dataRequest.toLowerCase().trim() + "%' ";
        } else if (dataRequest == "Status") {
            sql = "WHERE lower(issuestatus.pname) LIKE '%" + dataRequest.toLowerCase().trim() + "%' ";
        } else if (dataRequest == "All") {
            sql = "WHERE lower(PROJECT) LIKE '%" + dataRequest.toLowerCase().trim() + "%' ";
            sql += "OR lower(project.pname) LIKE '%" + dataRequest.toLowerCase().trim() + "%' ";
            sql += "OR lower(SUMMARY) LIKE '%" + dataRequest.toLowerCase().trim() + "%' ";
            sql += "OR lower(issuetype.pname) LIKE '%" + dataRequest.toLowerCase().trim() + "%' ";
            sql += "OR lower(issuestatus.pname) LIKE '%" + dataRequest.toLowerCase().trim() + "%' ";
        }
        let searchSQL: any = '';

        searchSQL = "SELECT PROJECT as pjID, project.pname as pjName ";
        searchSQL += "FROM (((jiraissue INNER JOIN project ON jiraissue.PROJECT = project.ID)";
        searchSQL += "INNER JOIN issuetype ON jiraissue.issuetype = issuetype.ID) ";
        searchSQL += "INNER JOIN issuestatus ON jiraissue.issuestatus = issuestatus.ID) " + sql + "GROUP BY project.pname";

        return new Promise((resolve, reject) => {
            this.databaseServeice.getConnection().query(searchSQL, (error, results, fields) => {
                if (error) {
                    reject(error);
                }
                resolve(results[0]);
            });
        })
    }
}