import { Injectable } from '@nestjs/common';
import { DatabaseService } from '../../services/database.service';

@Injectable()
export class ProjectBusiness {
    constructor(private readonly databaseServeice: DatabaseService) { }
    project(dataRequest: any): Promise<any> {
        let projectSQL: any = '';
        projectSQL = "SELECT pjID,pjName,stDone,stInProgress,stOpen,stDone+stInProgress+stOpen as Total FROM ";
        projectSQL += "(SELECT PJ.ID as pjID ,PJ.pname AS pjName ,IFNULL(sum(JI.issuestatus=10001),0) as stDone, IFNULL(sum(JI.issuestatus=3),0) as stInprogress,IFNULL(sum(JI.issuestatus=10000),0) as stOpen ";
        projectSQL += "FROM project PJ LEFT JOIN jiraissue  JI ON PJ.ID = JI.PROJECT  AND JI.issuetype IN (10000,10002,10003) ";
        projectSQL += "GROUP BY PJ.ID) T1 ";

        if (dataRequest == "Name") {
            projectSQL += "WHERE lower(T1.pjName) LIKE '%" + dataRequest.toLowerCase().trim() + "%' ";
        }

        // console.log(projectSQL);

        return new Promise((resolve, reject) => {
            this.databaseServeice.getConnection().query(projectSQL, (error, results, fields) => {
                if (error) {
                    reject(error);
                }
                resolve(results);
            });
        })
    }
}