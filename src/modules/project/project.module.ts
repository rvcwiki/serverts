import { Module } from '@nestjs/common';
import { ProjectController } from './project.controller';
import { ProjectBusiness } from './project.Business';
import { DatabaseService } from '../../services/database.service'
import { ServiceModule } from 'services/service.module';

@Module({
  imports: [ServiceModule],
  controllers: [ProjectController],
  providers: [ProjectBusiness]
})
export class ProjectModule { }
