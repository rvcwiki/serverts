import { Controller, Post, Body,Response } from '@nestjs/common';
import { ProjectBusiness } from './project.business';

@Controller('project')
export class ProjectController {
    constructor(private readonly Business: ProjectBusiness) { }

    @Post()
    project(@Response() res, @Body() data) {
        const dataRequest = data.dataRequest;
        this.Business.project(dataRequest).then(data => {
            res.status(200).json(data);
        }).catch(error => {

        })

        console.log("user : " + dataRequest);
       // res.status(200).json(data);
    }

}
