import { Injectable } from '@nestjs/common';
import { DatabaseService } from '../../services/database.service';
import { AuthenticationService } from '../../services/authentication.service';

@Injectable()
export class LoginBusiness {
    // private authenticationService: AuthenticationService;
    constructor(private readonly databaseServeice: DatabaseService,
        private readonly authenticationService: AuthenticationService) {
        // this.authenticationService = authenticationService;
    }
    login(email: string, username: string): Promise<any> {
        const userSQL = "SELECT ID, first_name, last_name FROM cwd_user WHERE email_address ='" + email + "' AND user_name = '" + username + "'";
        return new Promise((resolve, reject) => {
            this.databaseServeice.getConnection().query(userSQL, (error, results, fields) => {
                if (error) {
                    reject(error);
                }

                const token = this.authenticationService.createToken(email);
                results[0]['token'] = token;

                resolve(results[0]);
            });
        })
    }
}