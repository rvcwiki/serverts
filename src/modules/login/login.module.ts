import { Module } from '@nestjs/common';
import { LoginController } from './login.controller';
import { LoginBusiness } from './login.business';
import { ServiceModule } from 'services/service.module';

@Module({
    imports: [ServiceModule],
    controllers: [LoginController],
    providers: [LoginBusiness]
})
export class LoginModule {

}
