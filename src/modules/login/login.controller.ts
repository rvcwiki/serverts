import { Get, Post, Controller, Response, Body } from '@nestjs/common';
import { LoginBusiness } from './login.business';


@Controller('login')
export class LoginController {
    constructor(private readonly business: LoginBusiness) { }

    @Post()
    login(@Response() res, @Body() data) {
        const email = data.email;
        const username = data.username;
        
        this.business.login(email, username).then(data => {
            res.status(200).json(data);
        }).catch(error => {

        })
        // console.log("user : " + email);
        // console.log("pass : " + username);

       // res.status(200).json("dataaaaaaaaaaa");
    }


}
