import { Controller, Get, Response,Param} from '@nestjs/common';
import { EpicsBusiness } from './epics.business';

@Controller('epics')
export class EpicsController {
    constructor(private readonly Epics: EpicsBusiness){ }

    @Get('/:id')
    EpicsBusiness(@Response() res, @Param('id') data:any) {
        const id = data.id;
        this.Epics.epics(id).then(data => {
            res.status(200).json(data);
        }).catch(error => {

        })

        // res.status(200).json(data);
    }
}
