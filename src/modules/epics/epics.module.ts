import { Module } from '@nestjs/common';
import { EpicsController } from './epics.controller';
import { EpicsBusiness } from './epics.business';
import { ServiceModule } from 'services/service.module';

@Module({
  imports: [ServiceModule],
  controllers: [EpicsController],
  providers: [EpicsBusiness]
})
export class EpicsModule { }
