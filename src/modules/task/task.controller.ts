import { Get, Controller, Response, Param } from '@nestjs/common';
import { TaskBusiness } from './task.business';

@Controller('task')
export class TaskController {
    constructor(private readonly Business: TaskBusiness) { }

    @Get('/:id')
    task(@Response() res, @Param('id') data: any) {
        const id = data.id;
        this.Business.task(id).then(data => {
            res.status(200).json(data);
        }).catch(error => {

        })

        // res.status(200).json(data);
    }

}
