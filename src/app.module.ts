import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProjectModule } from './modules/project/project.module';
import { LoginModule } from './modules/login/login.module';
import { DatabaseService } from './services/database.service';
import { TaskController } from './modules/task/task.controller';
import { TaskModule } from './modules/task/task.module';
import { EpicsModule } from './modules/epics/epics.module';
import { SearchModule } from './modules/search/search.module';
import { ReleaseToolController } from './modules/release-tool/release-tool.controller';
import { ReleaseToolModule } from './modules/release-tool/release-tool.module';
import { ServiceModule } from 'services/service.module';

@Module({
  imports: [ProjectModule, LoginModule, ServiceModule, TaskModule, EpicsModule, SearchModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
