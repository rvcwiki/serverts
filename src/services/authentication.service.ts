import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class AuthenticationService {

    private SECRET: string = 'rvconnex';

    public createToken(email) {
        let token = jwt.sign({
            id: email
        }, this.SECRET, {
                expiresIn: 86400
            });
        return token;
    }
    public verifyToken(token, SECRET) {
        if (!token) {
            return false;
        } else {
            jwt.verify(token, SECRET, function (err, decoded) {
                if (err) {
                    return false;
                }
            })
        }


    }
}
