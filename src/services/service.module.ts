import { Module } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { DatabaseService } from './database.service';

@Module({
    providers: [DatabaseService, AuthenticationService],
    exports: [DatabaseService, AuthenticationService]
})
export class ServiceModule { }
